 #!/usr/bin/python
 # -*- coding: utf-8 -*-

"""Chame e executa o módulo restAPI"""
from restAPI import app
import sys

# Setando configuração de codificação
reload(sys)
sys.setdefaultencoding("utf-8")

app.run(debug=True)
