# coding: utf-8
"""
Este arquivo é uma model da tabela tarefas
no banco de dados em questão
"""
from restAPI import db, ma


class TodoDatabase(db.Model):
    """Model para tarefas"""
    __tablename__ = "tarefa"
    id = db.Column(db.Integer, primary_key=True, nullable=True)
    titulo = db.Column(db.String(50), nullable=True)
    descricao = db.Column(db.String(500), nullable=True)
    categoria = db.Column(db.String(50), nullable=True)


    def __init__(self, id, titulo, descricao, categoria):
        """Construtor da classe"""
        self.id = id
        self.titulo = titulo
        self.descricao = descricao
        self.categoria = categoria


class ToDoSchema(ma.ModelSchema):
    """Serializador Json da Classe TodoDatabase"""


    class Meta:
        """Meta"""
        model = TodoDatabase
        fields = ('id', 'titulo', 'descricao', 'categoria')

todos_schema = ToDoSchema(many=True)
