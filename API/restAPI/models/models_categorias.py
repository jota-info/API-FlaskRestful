# coding: utf-8
"""
Este arquivo é uma model da tabela categoria
no banco de dados em questão
"""
from restAPI import db, ma


class CategoriaDatabase(db.Model):
    """Classe de categorias"""
    __tablename__ = "categoria"
    nome = db.Column(db.String(50), primary_key=True, nullable=True)
    descricao = db.Column(db.String(500), nullable=True)


    def __init__(self, nome, descricao):
        self.nome = nome
        self.descricao = descricao

class CategoriaSchema(ma.ModelSchema):
    """Serializador Json da Classe CategoriaDatabase"""
    class Meta:
        """Meta"""
        model = CategoriaDatabase
        fields = ('nome', 'descricao')

categorias_schema = CategoriaSchema(many=True)
