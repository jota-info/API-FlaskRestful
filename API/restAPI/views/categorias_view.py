#!/usr/bin/env python
# -*- coding: utf-8 -*-8

"""
Este controller realiza ações sobre as categorias,
isso inclui cadastrar, alterar, deletar, visualizar e
visualizar tudo
"""
from restAPI import app, db
from flask_restful import reqparse, Resource, Api
from restAPI.models.models_categorias import CategoriaDatabase, categorias_schema
from flask import jsonify

api = Api(app)

def serializer():
    """Serializa os dados que são recebidos como parâmetros

    utilizando o formado Json
    """
    parser = reqparse.RequestParser()
    parser.add_argument('nome', type=str)
    parser.add_argument('descricao', type=str)
    return parser.parse_args()


class Categoria(Resource):
    """Métodos HTTP que necessitam da primary key como parâmetro"""
    def get(self, categoria):
        """Retorna uma categoria específica"""
        query = CategoriaDatabase.query.filter_by(nome=categoria)
        result = categorias_schema.dump(query)
        return result.data


    def delete(self, categoria):
        """Deleta uma categoria específica"""
        CategoriaDatabase.query.filter_by(nome=categoria).delete()
        db.session.commit()
        result = {'Status': "Sucesso", 'HTTP': 201}
        return jsonify(result)


    def put(self, categoria):
        """Altera uma categoria específica"""
        args = serializer()
        CategoriaDatabase.query.filter_by(nome=categoria).update({
            'nome': args['nome'].decode('utf-8'), 'descricao': args['descricao'].decode('utf-8')})
        db.session.commit()
        result = {'Status': "Sucesso", 'HTTP': 201}
        return jsonify(result)


class CategoriaList(Resource):
    """Métodos HTTP que não necessitam da primary key como parâmetro"""
    def get(self):
        """Retorna todas as categorias"""
        query = CategoriaDatabase.query.all()
        result = categorias_schema.dump(query)
        return result.data

    def post(self):
        """Insere uma nova categoria"""
        args = serializer()
        db.session.add(CategoriaDatabase(args['nome'].decode('utf-8'), args['descricao'].decode('utf-8')))
        db.session.commit()
        result = {'Status': "Sucesso", 'HTTP': 201}
        return jsonify(result)

api.add_resource(CategoriaList, '/categorias')
api.add_resource(Categoria, '/categorias/<categoria>')
