#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Inicializador do módulo restAPI
"""
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow


app = Flask('restAPI')
app = Flask('restAPI', instance_relative_config=True)
app.config.from_object('config.config')
app.config.from_pyfile('config.py')
db = SQLAlchemy(app)
db.text_factory = str
ma = Marshmallow(app)

# Define o Access-Control-Allow para Clientes terem acesso
@app.after_request
def after_request(response):
    """Ativação de permissões de acesso aos métodos HTTP"""
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add(
        'Access-Control-Allow-Headers', 'Content-Type,Authorization'
    )
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response

import restAPI.views.tarefas_view
import restAPI.views.categorias_view
