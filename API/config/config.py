# coding: utf-8
"""
Configuração do banco de dados
"""
from restAPI import app


app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///models/lista_tarefas.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
