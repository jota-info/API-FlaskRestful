# Documentação Lista de tarefas

### Esta é uma simples API para uma lista de tarefas, cujo principal objetivo é o aprendizado do framework FlaskRestful.

#### Como efetuar testes via cURL:

* curl http://localhost:{port}>/categorias/todos --- Lista todas as tarefas
* curl http://localhost:{port}>/categorias --- Lista todas as Categorias
* curl http://localhost:{port}/categorias/todos/{id tarefa} --- Lista uma tarefa específica
* * curl http://localhost:{port}/categorias/{NomeCategoria} --- Lista uma categoria específica
* curl -X POST -H "Content-Type: application/json" -d '{"titulo": "Exemplo titulo", "descricao": "exemplo_descricao", "categoria":"NomeCategoria"}' http://localhost:{port}/categorias/todos --- Adiciona uma nova tarefa
* curl -X POST -H "Content-Type: application/json" -d '{"nome": "ExemploNomeCategoria", "descricao": "exemplo_descricao"}' http://localhost:{port}/categorias --- Adiciona uma nova categoria
* curl -X DELETE http://localhost:{port}/categorias/todos/{id tarefa} --- Deleta uma tarefa específica
* curl -X DELETE http://localhost:{port}/categorias/{NomeCategoria} --- Deleta uma categoria específica
* curl -X PUT -H "Content-Type: application/json" -d '{"titulo": "Exemplo titulo", "descricao": "exemplo_descricao", "categoria":"Nome Categoria"}' http://localhost:{port}/categorias/todos/{id tarefa} --- Altera uma tarefa
* curl -X PUT -H "Content-Type: application/json" -d '{"nome": "ExemploNome", "descricao": "exemplo_descricao"}' http://localhost:{port}/categorias/{NomeCategoria} --- Altera uma categoria
